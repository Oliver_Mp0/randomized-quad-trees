#include <vector>
#include <string>
#include <bitset>
#include <ctgmath>
#include <cstdlib>
#include <iostream>
#include <chrono>
#include <ratio>
#include <queue>
#include "util.hh"

using namespace std;
using namespace std::chrono;


typedef vector<double> Key;
typedef vector<double> PMQuery;

#define maxK 10
time_point<high_resolution_clock> start;
time_point<high_resolution_clock> finish;
duration<double> elapsed;
double timee;
vector<pair<char,int> > input;
int K,n,n_d,n_t;
int cost;
bool incorrect;

vector<Key> InTree;

struct QuadtreeNode {
  Key x;
  vector<QuadtreeNode*> child;
  QuadtreeNode(const Key& _x) : x(_x),child(int(exp2(K)), NULL) {}
};

int subtree_to_insert(const Key& newKey, const Key& pKey);

QuadtreeNode* join(vector<QuadtreeNode*>& child);

void destroy_tree(QuadtreeNode*& t);


vector<QuadtreeNode*> split(const Key& x, QuadtreeNode*& T){
	vector<QuadtreeNode*> childs(int(exp2(K)),NULL);
	if( not T) return childs;
	vector<vector<QuadtreeNode*> > matrix(int(exp2(K)));
	queue<QuadtreeNode*> q;
	q.push(T);
	while(not q.empty()){
		QuadtreeNode* act= q.front(); q.pop(); ++cost;
		matrix[subtree_to_insert(act->x,x)].push_back(new QuadtreeNode(act->x));
		for(int j = 0; j<int(exp2(K)); ++j){
			if(act->child[j]){
				q.push(act->child[j]);
			}
		}		
	}
	for(int j = 0; j<int(exp2(K)); ++j)
		childs[j] = join(matrix[j]);
	return childs;
}


QuadtreeNode* join2(vector<QuadtreeNode*>& child){
	double max= -1;
	int v= -1;
	for(int w = 0; w<int(child.size()); ++w){
		if(child[w] and (child[w]->x)[K] > max ){ max= child[w]->x[K] ; v = w; ++cost;}
	}
	if( v== -1 ) return NULL;
	vector<vector<QuadtreeNode*> > matrix(int(exp2(K)));
	queue<QuadtreeNode*> q;
	for(int w = 0; w<int(child.size()); ++w){ 
		if(child[w]){
			if(w==v) {for(int j = 0; j<int(exp2(K)); ++j) if(child[v]->child[j]) q.push(child[v]->child[j]);}
			else q.push(child[w]);
		}
	}
	while(not q.empty()){
		QuadtreeNode* act= q.front(); q.pop(); ++cost;
		matrix[subtree_to_insert(act->x,child[v]->x)].push_back(new QuadtreeNode(act->x));
		for(int j = 0; j<int(exp2(K)); ++j){
			if(act->child[j]){
				q.push(act->child[j]);
			}
		}		
	}
	vector<QuadtreeNode*> childs(int(exp2(K)),NULL);
	for(int j = 0; j<int(exp2(K)); ++j)
		childs[j] = join(matrix[j]);
	QuadtreeNode* n= new QuadtreeNode(child[v]->x);
	n->child=childs;
	return n;
	
}

QuadtreeNode* join(vector<QuadtreeNode*>& child){
	double max= -1;
	int v= -1;
	for(int w = 0; w<int(child.size()); ++w){
		if(child[w] and (child[w]->x)[K] > max ){ max= child[w]->x[K] ; v = w; ++cost;}
	}
	if( v== -1 ) return NULL;
	else{
		QuadtreeNode* T = child[v];
		vector<vector<QuadtreeNode*> > matrix(int(exp2(K)));
		for(int i = 0; i<int(child.size()); ++i)
			if(i!=v){
				matrix[subtree_to_insert(child[i]->x,child[v]->x)].push_back(child[i]); ++cost;}
		for(int j = 0; j<int(exp2(K)); ++j){
			T->child[j] = join(matrix[j]);
		}
		return T;		
	}
}

int subtree_to_insert(const Key& newKey, const Key& pKey) {
    bitset<maxK> w;
    for (int i = 0; i < K; ++i)
        if (newKey[i] > pKey[i]) w[i] = 1;
    return int(w.to_ulong());
}

void insert(const Key& newKey, QuadtreeNode*& p) {
    if (not p) {
		p = new QuadtreeNode(newKey);
		InTree.push_back(newKey);
		//cout<<"insert: "; print_Kd_point(newKey);
	}
	else if(newKey[K] > (p->x)[K]){
		//cout<<"insertatroot: "; print_Kd_point(newKey);
		QuadtreeNode* n= new QuadtreeNode(newKey);
		n->child= split(newKey,p);
		destroy_tree(p);
		p = n;
		InTree.push_back(newKey);
	}
    else {
        int st = subtree_to_insert(newKey, p->x); ++cost;
        insert(newKey, p -> child[st]);
    }
}
// Releases memory allocated to the quad-tree t and its keys
void destroy_tree(QuadtreeNode*& t) {
    if (t) {
        for (int i = 0; i < (t->child).size(); ++i)
            destroy_tree(t->child[i]);
        delete t;
    }
    t = NULL;
}

void preorder(QuadtreeNode*& p,int deep){
	if (p) {
		for(int j=0;j<deep;j++) cout<<" "; cout<<deep<<": ";		
		print_Kd_point(p->x);
		for (int i = 0; i < (p->child).size(); ++i)
			preorder(p->child[i],deep+1);
	}
	else{
		for(int j=0;j<deep;j++) cout<<" "; cout<<deep<<": X\n";	
	}
}

void build_quad_tree(int n, QuadtreeNode*& p) {
        for (int i = 0; i < n; ++i) {
            Key newKey = vector_uniform(K+1);
            insert(newKey,p);
			//preorder(p,0);
        }
}



void delete_node(const Key& dKey, QuadtreeNode*& p) {
	++cost;
	if (dKey == p->x){
		auto q = join2(p->child);
		destroy_tree(p);
		p = q;
	}
	else{
		int st = subtree_to_insert(dKey, p->x);
		delete_node(dKey,p -> child[st]);
	}	
}

void run_experiment() {
	double tmicost,tmdcost,tccost,tmsi,tmsd,sc;
	tmicost,tmdcost,tccost,tmsi,tmsd,sc= 0;
    for (int t = 0; t < n_t; ++ t) {
		auto order= input;
        QuadtreeNode* p = NULL;
		cost= 0;
		start = high_resolution_clock::now();
        build_quad_tree(n,p);
		finish = high_resolution_clock::now();
		elapsed = finish - start;
		sc += elapsed.count();
		cout << K << "," << t << ",c" << ","<< cost << endl;
		tccost += cost; 
		incorrect=0;
		int icost,dcost,ni,nd;
		double si,sd;
		si=sd=0;
		icost=dcost=ni=nd=0;
		//preorder(p,0);
		for (int o = 0; o < order.size(); ++o) {
			char ord=order[o].first;
			n_d = order[o].second;
			if(ord=='i'){
				for (int i = 0; i < n_d; ++i) {
					Key newKey = vector_uniform(K+1);
					cost= 0;
					start = high_resolution_clock::now();
					insert(newKey,p);
					finish = high_resolution_clock::now();
					elapsed = finish - start;
					si += elapsed.count();
					cout << K << "," << t << ",i" << i << ","<< cost << endl;
					icost += cost;
					++ni;
					//preorder(p,0);
				}				
			}
			else if(ord=='d'){
				for (int i = 0; i < n_d; ++i) {
					int r = rand_int(InTree.size());
					const Key dKey = InTree[r]; //escoge random node InTree
					
					InTree[r]= InTree[InTree.size()-1];
					InTree.resize(InTree.size()-1);
					//cout<< "vamos a borrar: ";	print_Kd_point(dKey);
					cost= 0;
					start = high_resolution_clock::now();
					delete_node(dKey,p);
					finish = high_resolution_clock::now();	
					elapsed = finish - start;
					sd += elapsed.count();
					cout << K << "," << t << ",d" << i << ","<< cost << endl;
					dcost+=cost;
					++nd;
					//preorder(p,0);
				}				
			}
			else{
				cout<<"wrong input"<<endl;
				exit(1);
			}
		}
		//preorder(p,0);
        destroy_tree(p);
		InTree= vector<Key> (0);
		double mi= (icost*1.0)/(ni*1.0);
		tmicost+=mi;
		double msi= (si*1.0)/(ni*1.0);
		tmsi+=msi;
		double md= (dcost*1.0)/(nd*1.0);
		tmdcost+=md;
		double msd= (sd*1.0)/(nd*1.0);
		tmsd+=msd;
		cout<< "ticost:" << icost << " micost:" << mi << " tdcost:" << dcost << " mdcost:" << md <<endl;
		cout<< "si:" << si << " msi:" << msi << " sd:" << sd << " msd:" << msd <<endl;
		if(incorrect) cout<<"INCORRECT\n";
		else cout<<"CORRECT\n";
    }
	double mmi,mmd,mc,mmsi,mmsd,msc;
	mc= (tccost)/(n_t*1.0);
	msc= (sc)/(n_t*1.0);
	mmi= (tmicost)/(n_t*1.0);
	mmd= (tmdcost)/(n_t*1.0);
	mmsi= (tmsi)/(n_t*1.0);
	mmsd= (tmsd)/(n_t*1.0);
	cout<< "mc:" << mc << " mmi:" << mmi << " mmd:" << mmd << " msc:" << msc << " mmsi:" << mmsi << " mmsd:" << mmsd <<endl;
}

int main() {
    srand (time(NULL));
	timee=0;
    cin >> K;
	cin >> n_t;
    cin >> n;
	char mander;
	while(cin >>mander >> n_d) input.push_back({mander,n_d}); 
    run_experiment();
}
