Para ejecutar los codigos compilarlos y editad el script con el nombre del ejecutable que querais probar.
los programas usan este formato de input:
2 300 25000 i 20 d 20 i 14 d 14
el primer parametro es la dimension
el segundo es el numero de arboles a crear
el tercero es el numero de nodos por arbol
por ultimo una lista de instrucciones que consisten en una pareja donde el primer parametro
es "i" de insercion o "d" de delete y el segundo parametro es un numero de nodos

el output consiste en el coste de cada insercion y borrado por cada fila
Al final del todo se hace la media del coste en nodos visitados en la creacion, la insercion
y el borrado asi como su tiempo de ejecucion
mc:1.45187e+006 mmi:265376 mmd:264877 msc:0.565794 mmsi:0.0735225 mmsd:0.0730628
mc es el coste medio de creacion
mmi es el coste medio de insercion
mmd es el coste medio de delete
msc es el tiempo medio de creacion en segundos
mmsi es el tiempo medio de insertado en segundos
mmsd es el tiempo medio de delete en segundos